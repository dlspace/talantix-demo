# %%
import json
import random
from functools import partial
from pathlib import Path

import albumentations as A
import cv2
import numpy as np
import torch
from albumentations.pytorch.transforms import ToTensorV2
from matplotlib import pyplot as plt
from matplotlib.pylab import permutation
from talantix.data.split_train_test import make_pair_list


# %%
def patchify_image(image: np.array, patch_size: int | list, **kwargs) -> np.array:
    shape = image.shape
    if isinstance(patch_size, int):
        patch_size = [patch_size, patch_size]
    rs = row_size = patch_size[0]
    cs = col_size = patch_size[1]
    rows = np.ceil(shape[0] / row_size).astype(np.int8)
    cols = np.ceil(shape[1] / col_size).astype(np.int8)

    patches = []
    for i_row in range(rows):
        for i_col in range(cols):
            rb, re = i_row * rs, i_row * rs + rs  # row_begin, row_end
            cb, ce = i_col * cs, i_col * cs + cs  # col_begin, col_end
            patch = image[rb : min(shape[0], re), cb : min(shape[1], ce), ...]
            if patch.shape[0] < row_size or patch.shape[1] < col_size:
                pad_width = []
                pad_width.append([0, row_size - patch.shape[0]])
                pad_width.append([0, col_size - patch.shape[1]])
                for j in range(2, len(shape)):
                    pad_width.append([0, 0])
                patch = np.pad(
                    patch,
                    pad_width=pad_width,
                    mode="constant",
                    constant_values=0,
                )
            patches.append(patch)
    return np.stack(patches, axis=0)


# if __name__ == "__main__":
#     image = np.random.uniform(0, 1, size=(401, 1401, 3))
#     img = patchify_image(image, patch_size=200)
#     print(img.shape)

#     image = np.random.uniform(0, 1, size=(401, 1401, 1))
#     img = patchify_image(image, patch_size=200)
#     print(img.shape)

#     image = np.random.uniform(0, 1, size=(401, 1401))
#     img = patchify_image(image, patch_size=200)
#     print(img.shape)


# %%
def make_gradient_2d(start, stop, width, height, is_horizontal=True, **kwargs):
    if is_horizontal:
        return np.tile(np.linspace(start, stop, width), (height, 1))
    else:
        return np.tile(np.linspace(start, stop, height), (width, 1)).T


def make_gradient_3d(
    start_list, stop_list, width, height, is_horizontal_list, **kwargs
):
    result = np.zeros((height, width, len(start_list)), dtype=float)

    for i, (start, stop, is_horizontal) in enumerate(
        zip(start_list, stop_list, is_horizontal_list)
    ):
        result[:, :, i] = make_gradient_2d(start, stop, width, height, is_horizontal)

    return result


def concatenate_gradients_to_patches(patches, patch_size: int | list, **kwargs):
    shape = patches.shape
    ps = patch_size  # hor and vert sizes
    ps = [ps, ps] if isinstance(ps, int) else ps

    if len(shape) == 3:
        h = make_gradient_2d(0, 1, ps[0], ps[1], True)
        v = make_gradient_2d(0, 1, ps[0], ps[1], False)
    if len(shape) == 4:
        n = shape[-1]  # number of colors
        h = make_gradient_3d((0,) * n, (1,) * n, ps[0], ps[1], (True,) * n)
        v = make_gradient_3d((0,) * n, (1,) * n, ps[0], ps[1], (False,) * n)
    h = h[np.newaxis, ...]
    v = v[np.newaxis, ...]

    return np.concatenate((patches, h, v), axis=0)


# if __name__ == "__main__":
#     image = np.random.uniform(0, 1, size=(400, 1400, 3))
#     patches = patchify_image(image, patch_size=200)
#     patches = cat_gradients_to_patches(patches)
#     print(patches.shape)

#     image = np.random.uniform(0, 1, size=(400, 1400, 1))
#     patches = patchify_image(image, patch_size=200)
#     patches = cat_gradients_to_patches(patches)
#     print(patches.shape)

#     image = np.random.uniform(0, 1, size=(400, 1400))
#     patches = patchify_image(image, patch_size=200)
#     patches = cat_gradients_to_patches(patches)
#     print(patches.shape)

#     plt.figure(figsize=(10, 8))
#     plt.imshow(patches[-2, ...])
#     plt.show()

#     plt.figure(figsize=(10, 8))
#     plt.imshow(patches[-1, ...])
#     plt.show()


# %%
def flatten_the_mask(masks, axis: int = 0, **kwargs):
    index = masks.any(axis=(0, 1)).argmax()
    mask = masks.any(axis=-1)
    # index = masks.sum(axis=(0, 1)).argmax()
    mask = np.where(mask, index + 1, mask)
    return mask


def channel_shuffle(img, channels_shuffled):
    img = img[..., channels_shuffled]
    return img


class ChannelPermutaion(A.DualTransform):
    # A.ChannelShuffle but for mask too
    def __init__(
        self, n_first_channels: int, always_apply: bool = False, p: float = 0.5
    ):
        super().__init__(always_apply, p)
        self.n_first_channels = n_first_channels

    @property
    def targets_as_params(self):
        return ["image"]

    def apply(self, img, channels_shuffled=(0, 1, 2), **params):
        channels = [*channels_shuffled, *range(len(channels_shuffled), img.shape[2])]
        return channel_shuffle(img, channels)

    def apply_to_mask(self, img, channels_shuffled=(0, 1, 2), **params):
        channels = [*channels_shuffled, *range(len(channels_shuffled), img.shape[2])]
        return channel_shuffle(img, channels_shuffled)

    def get_params_dependent_on_targets(self, params):
        img = params["image"]
        ch_arr = list(range(self.n_first_channels))
        random.shuffle(ch_arr)
        return {"channels_shuffled": ch_arr}

    def get_transform_init_args_names(self):
        return ()


# %%
class CoordinatesDataset(torch.utils.data.Dataset):
    """Custom CoordinatesDataset for capcha task"""

    def __init__(self, path: str, classes: dict, transform: A.Compose = None):
        self.classes = classes
        self.path = path
        self.transform = transform
        self.pair_list = make_pair_list(path)
        self.patch_size = 200

    def __getitem__(self, index):
        fn_image, fn_json = self.pair_list[index]
        image = plt.imread(str(fn_image), "jpg")
        mask = np.zeros_like(image)
        with open(fn_json) as json_file:
            json_data = json.load(json_file)

        shapes = {s["label"]: s["points"] for s in json_data["shapes"]}
        shapes["+"] = (
            [p - 1 for p in shapes["+"][0]],
            [p + 1 for p in shapes["+"][0]],
        )  # make square
        answ = int(np.floor(shapes["+"][0][0] / self.patch_size))
        counter = 0
        for label, shape in shapes.items():
            if "_" not in label:
                continue
            if counter == answ:
                (y0, x0), (y1, x1) = shape[0], shape[1]
                x0, y0, x1, y1 = round(x0), round(y0), round(x1), round(y1)
                mask[x0:x1, y0:y1, :] = (1, 1, 1)
            counter += 1
        if self.transform is not None:
            transformed = self.transform(image=image, mask=mask)
            image, mask = transformed["image"], transformed["mask"]
        # return raw_image, image, json_data, mask
        return image, mask

    def __len__(self):
        return len(self.pair_list)


if __name__ == "__main__":
    params = dict()
    params["path_project"] = Path(
        "/home/rinkorn/space/prog/python/free/project-talantix-demo/"
    )
    params["path_dataset"] = (
        params["path_project"]
        / "data/processed/mixed_train_to_the_coordinates_dataset/"
    )
    params["mppxl_current"] = 1.0
    # params["mppxl_target"] = 1.56
    params["mppxl_target"] = 0.83
    params["scale_factor"] = params["mppxl_current"] / params["mppxl_target"]
    params["mean"] = 0.21568627450980393
    params["std"] = 0.2
    params["classes"] = {f"{i}": i for i in range(8)}  # background + 7 images
    params["max_pixel_value"] = 255.0
    params["interpolation"] = cv2.INTER_AREA
    params["patch_size"] = 200
    params["transform"] = A.Compose(
        [
            A.Normalize(
                mean=params["mean"],
                std=params["std"],
                max_pixel_value=params["max_pixel_value"],
            ),
            A.Lambda(
                name="get_one_color",
                image=lambda image, **kwargs: np.max(image, axis=-1),
                mask=lambda masks, **kwargs: masks[:, :, 0],
                always_apply=True,
            ),
            A.Lambda(
                name="patchify",
                image=partial(patchify_image, patch_size=params["patch_size"]),
                mask=partial(patchify_image, patch_size=params["patch_size"]),
                always_apply=True,
            ),
            A.Lambda(
                name="choose_patches",
                image=lambda images, **kwargs: images[:8, ...],
                mask=lambda masks, **kwargs: masks[:7, ...],
                always_apply=True,
            ),
            A.Lambda(
                name="concatenate_gradients_to_patches",
                image=partial(
                    concatenate_gradients_to_patches,
                    patch_size=params["patch_size"],
                ),
                always_apply=True,
            ),
            A.Lambda(
                name="moveaxis_in_images",
                image=lambda images, **kwargs: np.moveaxis(images, 0, -1),
                always_apply=True,
            ),
            A.Lambda(
                name="moveaxis_in_masks",
                mask=lambda masks, **kwargs: np.moveaxis(masks, 0, -1),
                always_apply=True,
            ),
            ChannelPermutaion(
                n_first_channels=7,
                always_apply=True,
            ),
            A.Lambda(
                name="flatten",
                mask=flatten_the_mask,
                always_apply=True,
            ),
            # A.Rotate(
            #     limit=(-5, 5),
            #     interpolation=params["interpolation"],
            #     border_mode=cv2.BORDER_REFLECT_101,
            #     always_apply=True,
            # ),
            # A.RandomRotate90(p=0.5),
            # A.Flip(p=0.5),
            A.RandomScale(
                scale_limit=[
                    params["scale_factor"] - 1.0,
                    params["scale_factor"] - 1.0,
                ],
                interpolation=params["interpolation"],
                always_apply=True,
            ),
            A.Lambda(
                name="to_float32",
                image=lambda images, **kwargs: images.astype(np.float32),
                mask=lambda masks, **kwargs: masks.astype(np.float32),
                always_apply=True,
            ),
            # ToTensorV2(),
        ]
    )
    train_dataset = CoordinatesDataset(
        params["path_dataset"],
        params["classes"],
        params["transform"],
    )
    train_dataloader = torch.utils.data.DataLoader(
        dataset=train_dataset,
        batch_size=10,
        shuffle=True,
        pin_memory=True,
        num_workers=0,
    )

    # for step, (image, mask) in enumerate(train_dataloader):
    #     # image = torch.unsqueeze(image, dim=1)
    #     mask = torch.unsqueeze(mask, dim=1)
    #     print(image.dtype)
    #     print(mask.dtype)
    #     print(image.shape)
    #     print(mask.shape)
    #     plot_sample(image)
    #     print(image.shape)
    #     print(mask.shape)
    #     print(image.mean())
    #     print(image.std())
    #     break


if __name__ == "__main__":
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    # image, patches, json_data, masks = train_dataset[127]
    for i in range(200, 201):
        patches, masks = train_dataset[160]

        answ = int(masks.max())
        # print(f"res answ: {answ}")

        # for i in range(patches.shape[-1]):
        for i in [answ - 1]:
            patch = patches[..., i]
            # mask = masks[..., i] if (i < masks.shape[-1]) else None
            mask = masks
            fig, ax = plt.subplots(1, 2, figsize=(7, 4))
            ax[0].imshow(patch)
            if mask is not None:
                h = ax[1].imshow(mask, cmap="bone")
                divider = make_axes_locatable(ax[1])
                cax = divider.append_axes("right", size="5%", pad=0.05)
                fig.colorbar(h, cax=cax, orientation="vertical")
            plt.show()

        print(patches.dtype, patches.shape)
        print(masks.dtype, masks.shape)


# if __name__ == "__main__":
#     for _, batch in enumerate(train_dataloader):
#         imgs = batch[0]
#         msks = batch[1]
#         break

#     print(imgs.dtype, imgs.shape)
#     print(msks.dtype, msks.shape)

# # %%
# if __name__ == "__main__":
#     image, patches, json_data, masks = train_dataset[122]
#     plt.figure(figsize=(10, 8))
#     plt.imshow(image[0, :, :, 0], vmin=0.0, vmax=1.0)
#     plt.show()

#     for shape in json_data["shapes"]:
#         label = shape["label"]
#         points = shape["points"]
#         if label == "+":
#             points = [p - 6 for p in points[0]], [p + 6 for p in points[0]]
#         print(label, points)
#         (y0, x0), (y1, x1) = points[0], points[1]
#         x0, y0, x1, y1 = round(x0), round(y0), round(x1), round(y1)
#         mask = image.copy()
#         mask[0, x0:x1, y0:y1, 0] = (32, 200, 32)

#         plt.figure(figsize=(10, 8))
#         plt.imshow(mask, vmin=0.0, vmax=1.0)
#         plt.show()


# %%
