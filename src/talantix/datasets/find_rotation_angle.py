from pathlib import Path

import cv2
import numpy as np


def rotate_image(image, angle):
    # Получение размеров изображения
    height, width = image.shape[:2]
    # Вычисление центра изображения
    center = (width // 2, height // 2)
    # Создание матрицы преобразования и вращение изображения
    rotation_matrix = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated_image = cv2.warpAffine(image, rotation_matrix, (width, height))
    return rotated_image


def find_rotation_angle(image):
    # Преобразование в оттенки серого
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Применение размытия для снижения шума
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    # Выделение краев с использованием оператора Canny
    edges = cv2.Canny(blurred, 10, 100)
    # Поиск линий на изображении
    lines = cv2.HoughLinesP(
        edges,
        1,
        # np.pi / 180,
        np.pi / 360,
        threshold=30,
        minLineLength=40,
        maxLineGap=8,
    )
    # plt.figure(figsize=(5, 5))
    # plt.imshow(edges)
    # for line in lines:
    #     x1, y1, x2, y2 = line[0]
    #     angle = np.arctan2(y2 - y1, x2 - x1) * 180.0 / np.pi
    #     if abs(angle) > 45:
    #         angle = angle - np.sign(angle) * 90
    #     if abs(angle) > 8:
    #         continue
    #     plt.plot([x1, x2], [y1, y2], "r")
    # plt.show()
    # Вычисление угла наклона
    angles = []
    for line in lines:
        x1, y1, x2, y2 = line[0]
        angle = np.arctan2(y2 - y1, x2 - x1) * 180.0 / np.pi
        if abs(angle) > 45:
            angle = angle - np.sign(angle) * 90
        if abs(angle) > 8:
            continue
        angles.append(angle)

    # Усреднение углов для получения общего угла наклона
    average_angle = np.mean(angles)
    return average_angle


# %%
if __name__ == "__main__":
    path_project = Path("/home/rinkorn/space/prog/python/free/project-talantix-demo/")
    path_dataset = path_project / "data/processed/train_to_the_coordinates_dataset/"

    for i in range(2626, 4000):
        image_path = path_dataset / f"{i}.jpg"
        if not image_path.exists():
            continue

        image = cv2.imread(str(image_path))
        image = image[:200, :200, :]

        rotation_angle = find_rotation_angle(image)
        print(f"Угол наклона: {rotation_angle} градусов")

        rotated_image = rotate_image(image, rotation_angle)

        import matplotlib.pyplot as plt

        plt.figure(figsize=(10, 8))
        plt.subplot(121)
        plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
        plt.title("Исходное изображение")
        plt.subplot(122)
        plt.imshow(cv2.cvtColor(rotated_image, cv2.COLOR_BGR2RGB))
        plt.title("Повернутое изображение")

        plt.show()
