import abc

import albumentations as A
import cv2
from talantix.core.ICommand import ICommand


class IScalableSample(abc.ABC):
    @abc.abstractmethod
    def get_scale_factor(self):
        pass

    @abc.abstractmethod
    def get_interpolation(self):
        pass

    @abc.abstractmethod
    def get_image(self):
        pass

    @abc.abstractmethod
    def get_mask(self):
        pass

    @abc.abstractmethod
    def set_image(self, value):
        pass

    @abc.abstractmethod
    def set_mask(self, value):
        pass


class ScaleSampleCmd(ICommand):
    """Interested link: https://robocraft.ru/computervision/3956"""

    def __init__(self, o: IScalableSample):
        self.o = o

    def execute(self):
        # scale_factor = mppxl_current / mppxl_target
        scale_factor = self.o.get_scale_factor()
        augmentation = A.RandomScale(
            scale_limit=[scale_factor - 1.0, scale_factor - 1.0],
            interpolation=cv2.INTER_AREA,
            p=1,
        )
        data_dict = augmentation(
            image=self.o.get_image(),
            mask=self.o.get_mask(),
        )
        self.o.set_image(data_dict["image"])
        self.o.set_mask(data_dict["mask"])


if __name__ == "__main__":
    import numpy as np
    from talantix.core.Adapter import Adapter
    from talantix.core.UObject import UObject

    obj = UObject()
    obj["scale_factor"] = 1.0 / 2.0
    obj["interpolation"] = cv2.INTER_AREA
    obj["image"] = np.random.uniform(0, 1, [25, 30])
    obj["mask"] = None

    scalable_obj = Adapter.generate(IScalableSample)(obj)
    cmd = ScaleSampleCmd(scalable_obj)
    cmd.execute()

    print(obj["image"].shape)
    print(obj["mask"])
