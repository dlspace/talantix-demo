import random

import albumentations as A


class ChannelPermutaion(A.DualTransform):
    # A.ChannelShuffle
    @property
    def targets_as_params(self):
        return ["image"]

    def apply(self, img, channels_shuffled=(0, 1, 2), **params):
        return A.F.channel_shuffle(img, channels_shuffled)

    def get_params_dependent_on_targets(self, params):
        img = params["image"]
        ch_arr = list(range(img.shape[2]))
        random.shuffle(ch_arr)
        return {"channels_shuffled": ch_arr}

    def get_transform_init_args_names(self):
        return ()


if __name__ == "__main__":
    import numpy as np
    from talantix.core.Adapter import Adapter
    from talantix.core.UObject import UObject

    obj = UObject()
    obj["scale_factor"] = 1.0 / 2.0
    obj["interpolation"] = cv2.INTER_AREA
    obj["image"] = np.random.uniform(0, 1, [25, 30])
    obj["mask"] = None

    cmd = ChannelPermutaion(obj)
    cmd.execute()

    print(obj["image"].shape)
    print(obj["mask"])
