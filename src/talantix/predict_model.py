# %%
from collections import defaultdict
from functools import partial
from pathlib import Path

import albumentations as A
import cv2
import numpy as np
import segmentation_models_pytorch as smp
import torch
from albumentations.pytorch.transforms import ToTensorV2
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from talantix.core.ICommand import ICommand
from talantix.core.UObject import UObject
from talantix.datasets.CoordinatesDataset import (
    ChannelPermutaion,
    CoordinatesDataset,
    concatenate_gradients_to_patches,
    flatten_the_mask,
    patchify_image,
)
from talantix.train_model import InitTrainableObjectCmd


class InitPredictableObjectCmd(ICommand):
    def __init__(self, o):
        self.o = o

    def execute(self):
        self.o["path.models.state_dict"] = self.o["path.models"] / "model_state.pth"
        self.o["path.dataset.test"] = self.o["path.project"] / "data/processed/test/"
        self.o["device.test"] = "cuda:0"
        self.o["batch_size.test"] = 1
        self.o["transforms.test"] = A.Compose(
            [
                A.Normalize(
                    mean=self.o["mean"],
                    std=self.o["std"],
                    max_pixel_value=self.o["max_pixel_value"],
                ),
                A.Lambda(
                    name="make_layer",
                    image=lambda image, **kwargs: np.max(image, axis=-1),
                    mask=lambda masks, **kwargs: masks[:, :, 0],
                    always_apply=True,
                ),
                A.Lambda(
                    name="patchify",
                    image=partial(patchify_image, patch_size=self.o["patch_size"]),
                    mask=partial(patchify_image, patch_size=self.o["patch_size"]),
                    always_apply=True,
                ),
                A.Lambda(
                    name="choose_patches",
                    image=lambda images, **kwargs: images[:8, ...],
                    mask=lambda masks, **kwargs: masks[:7, ...],
                    always_apply=True,
                ),
                A.Lambda(
                    name="concatenate_gradients_to_patches",
                    image=partial(
                        concatenate_gradients_to_patches,
                        patch_size=self.o["patch_size"],
                    ),
                    always_apply=True,
                ),
                A.Lambda(
                    name="moveaxis",
                    image=lambda images, **kwargs: np.moveaxis(images, 0, -1),
                    mask=lambda masks, **kwargs: np.moveaxis(masks, 0, -1),
                    always_apply=True,
                ),
                ChannelPermutaion(
                    n_first_channels=7,
                    always_apply=True,
                ),
                A.Lambda(
                    name="flatten",
                    mask=flatten_the_mask,
                    always_apply=True,
                ),
                A.RandomScale(
                    scale_limit=[
                        self.o["fn_scale_factor"]() - 1.0,
                        self.o["fn_scale_factor"]() - 1.0,
                    ],
                    interpolation=self.o["interpolation"],
                    always_apply=True,
                ),
                A.Lambda(
                    name="to_float32",
                    image=lambda images, **kwargs: images.astype(np.float32),
                    mask=lambda masks, **kwargs: masks.astype(np.float32),
                    always_apply=True,
                ),
                ToTensorV2(),
            ]
        )
        self.o["dataset.test"] = CoordinatesDataset(
            self.o["path.dataset.test"],
            self.o["classes"],
            self.o["transforms"],
        )
        self.o["dataloader.test"] = torch.utils.data.DataLoader(
            dataset=self.o["dataset.test"],
            batch_size=self.o["batch_size"],
            shuffle=True,
            pin_memory=True,
            num_workers=4,
            drop_last=True,
        )


class LoadStateDictCmd(ICommand):
    def __init__(self, o):
        self.o = o

    def execute(self):
        # model = LitModel(1, 128, 11)
        model = self.o["model"]
        state_dict = torch.load(self.o["path.models.state_dict"])
        model.load_state_dict(state_dict)
        model.eval()
        # model = torch.jit.load(path_in)
        # model = onnx.load(str(path_in))
        # # Check that the model is well formed
        # onnx.checker.check_model(model)
        # # Print a human readable representation of the graph
        # print(onnx.helper.printable_graph(model.graph))
        # # model = torch.load(str(path_in))
        self.o["model"] = model


class PredictSamplesCmd(ICommand):
    def __init__(self, o):
        self.o = o

    def execute(self):
        # torch.set_float32_matmul_precision("medium")
        device = self.o["device.test"]
        model = self.o["model"]
        dataloader = self.o["dataloader.test"]

        model.to(device)
        model.train(False)
        with torch.no_grad():
            image, mask = next(iter(dataloader))
            image = image.to(device)
            pred = model(image.float())

            img = image[0, ...].permute(1, 2, 0).cpu().detach().numpy()
            mask_true = mask[0, ...].cpu().detach().numpy()
            mask_pred = pred[0, ...].softmax(0)
            mask_pred = mask_pred.permute(1, 2, 0).cpu().detach().numpy()
            # mask_pred = pred[0, ...].argmax(0).unsqueeze(0).unsqueeze(0)

            # image = image[0, ...].permute(1, 2, 0).cpu().detach().numpy()
            # mask = mask[0, ...].permute(1, 2, 0).cpu().detach().numpy()

            fig, ax = plt.subplots(2, 8, figsize=(28, 6))
            for i_col in range(7):
                hax = ax[0, i_col].imshow(img[..., i_col])
                divider = make_axes_locatable(ax[0, i_col])
                cax = divider.append_axes("right", size="5%", pad=0.05)
                fig.colorbar(hax, cax=cax, orientation="vertical")
                hax = ax[1, i_col].imshow(mask_pred[..., i_col + 1], vmin=0.0, vmax=1.0)
                divider = make_axes_locatable(ax[1, i_col])
                cax = divider.append_axes("right", size="5%", pad=0.05)
                fig.colorbar(hax, cax=cax, orientation="vertical")
            ax[0, 7].imshow(img[..., 7])
            divider = make_axes_locatable(ax[0, 7])
            cax = divider.append_axes("right", size="5%", pad=0.05)
            fig.colorbar(hax, cax=cax, orientation="vertical")
            ax[1, 7].imshow(mask_true, vmin=0.9, vmax=1.0)
            divider = make_axes_locatable(ax[1, 7])
            cax = divider.append_axes("right", size="5%", pad=0.05)
            fig.colorbar(hax, cax=cax, orientation="vertical")
            plt.show()


if __name__ == "__main__":
    obj = UObject()
    InitTrainableObjectCmd(obj).execute()
    InitPredictableObjectCmd(obj).execute()
    LoadStateDictCmd(obj).execute()
    for i in range(10):
        PredictSamplesCmd(obj).execute()
