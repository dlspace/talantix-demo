import torch

from talantix.core.ICommand import ICommand


class CheckCudaCmd(ICommand):
    def execute(self):
        print(torch.__version__)
        print(torch.cuda.device_count())
        print(torch.cuda.is_available())
        print(torch.cuda.get_device_name(0))
        print(torch.backends.cudnn.m.is_available())
        print(torch.backends.cudnn.version())


if __name__ == "__main__":
    CheckCudaCmd().execute()
