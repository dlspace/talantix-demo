import abc


class IUObject(abc.ABC):
    @abc.abstractmethod
    def __getitem__(self, attr):
        pass

    @abc.abstractmethod
    def __setitem__(self, attr, value):
        pass


class UObject(IUObject):
    def __init__(self):
        self._state = {}

    def __getitem__(self, attr):
        return self._state[attr]

    def __setitem__(self, attr, value):
        self._state[attr] = value
