# %%
from collections import defaultdict
from functools import partial
from pathlib import Path

import albumentations as A
import cv2
import numpy as np
import segmentation_models_pytorch as smp
import torch
from albumentations.pytorch.transforms import ToTensorV2
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

from talantix.common.plot_sample import plot_sample
from talantix.core.ICommand import ICommand
from talantix.core.UObject import UObject
from talantix.datasets.CoordinatesDataset import (
    ChannelPermutaion,
    CoordinatesDataset,
    concatenate_gradients_to_patches,
    flatten_the_mask,
    patchify_image,
)
from talantix.models.SimpleCNN import SimpleCNN


# %%
class InitTrainableObjectCmd(ICommand):
    def __init__(self, o):
        self.o = o

    def execute(self):
        self.o["path.project"] = Path(
            "/home/rinkorn/space/prog/python/free/project-talantix-demo/"
        )
        self.o["path.dataset.train"] = self.o["path.project"] / "data/processed/train/"
        self.o["path.models"] = self.o["path.project"] / "models"
        self.o["epochs"] = 500
        self.o["batch_size"] = 16
        self.o["device"] = "cuda:0"
        self.o["mppxl_current"] = 1.0
        self.o["mppxl_target"] = 1.56
        # self.o["mppxl_target"] = 0.78
        self.o["fn_scale_factor"] = (
            lambda *args: self.o["mppxl_current"] / self.o["mppxl_target"]
        )
        self.o["mean"] = 0.21568627450980393
        self.o["std"] = 0.2
        self.o["classes"] = {f"{i}": i for i in range(8)}  # background + 7 images
        self.o["max_pixel_value"] = 255.0
        self.o["interpolation"] = cv2.INTER_AREA
        self.o["patch_size"] = 200
        self.o["transforms"] = A.Compose(
            [
                A.Normalize(
                    mean=self.o["mean"],
                    std=self.o["std"],
                    max_pixel_value=self.o["max_pixel_value"],
                ),
                A.Lambda(
                    name="make_layer",
                    # image=lambda image, **kwargs: image[:, :, -1],
                    # image=lambda image, **kwargs: image[:,:,np.random.randint(0, 2 + 1)],
                    # image=lambda image, **kwargs: np.mean(image, axis=-1),
                    image=lambda image, **kwargs: np.max(image, axis=-1),
                    # image=lambda image, **kwargs: np.min(image, axis=-1),
                    # image=lambda image, **kwargs: np.abs(image[..., -1] - image[..., 1]),
                    mask=lambda masks, **kwargs: masks[:, :, 0],
                    always_apply=True,
                ),
                A.Lambda(
                    name="patchify",
                    image=partial(patchify_image, patch_size=self.o["patch_size"]),
                    mask=partial(patchify_image, patch_size=self.o["patch_size"]),
                    always_apply=True,
                ),
                A.Lambda(
                    name="choose_patches",
                    image=lambda images, **kwargs: images[:8, ...],
                    mask=lambda masks, **kwargs: masks[:7, ...],
                    always_apply=True,
                ),
                A.Lambda(
                    name="concatenate_gradients_to_patches",
                    image=partial(
                        concatenate_gradients_to_patches,
                        patch_size=self.o["patch_size"],
                    ),
                    always_apply=True,
                ),
                A.Lambda(
                    name="moveaxis",
                    image=lambda images, **kwargs: np.moveaxis(images, 0, -1),
                    mask=lambda masks, **kwargs: np.moveaxis(masks, 0, -1),
                    always_apply=True,
                ),
                ChannelPermutaion(
                    n_first_channels=7,
                    always_apply=True,
                ),
                A.Lambda(
                    name="flatten",
                    mask=flatten_the_mask,
                    always_apply=True,
                ),
                # A.Rotate(
                #     limit=(-5, 5),
                #     interpolation=self.o["interpolation"],
                #     border_mode=cv2.BORDER_REFLECT_101,
                #     always_apply=True,
                # ),
                # A.RandomRotate90(p=0.5),
                # A.Flip(p=0.5),
                A.RandomScale(
                    scale_limit=[
                        self.o["fn_scale_factor"]() - 1.0,
                        self.o["fn_scale_factor"]() - 1.0,
                    ],
                    interpolation=self.o["interpolation"],
                    always_apply=True,
                ),
                A.Lambda(
                    name="to_float32",
                    image=lambda images, **kwargs: images.astype(np.float32),
                    mask=lambda masks, **kwargs: masks.astype(np.float32),
                    always_apply=True,
                ),
                ToTensorV2(),
            ]
        )
        self.o["dataset.train"] = CoordinatesDataset(
            self.o["path.dataset.train"],
            self.o["classes"],
            self.o["transforms"],
            # transform=None,
        )
        self.o["dataloader.train"] = torch.utils.data.DataLoader(
            dataset=self.o["dataset.train"],
            batch_size=self.o["batch_size"],
            shuffle=True,
            pin_memory=True,
            num_workers=4,
            drop_last=True,
        )
        # self.o["model"] = SimpleCNN(
        #     n_in=10,
        #     n_latent=128,
        #     n_out=8,
        # )
        self.o["model"] = smp.Unet(
            in_channels=10,
            classes=8,
            encoder_depth=5,
            decoder_channels=[128, 128, 128, 128, 128],
        )
        self.o["optimizer"] = torch.optim.AdamW(
            self.o["model"].parameters(),
            lr=3e-4,
            betas=(0.9, 0.999),
            eps=1e-8,
            weight_decay=1e-2,
            amsgrad=False,
        )
        self.o["loss_fn"] = smp.losses.DiceLoss(
            mode="multiclass",
            classes=8,
            log_loss=False,
            from_logits=True,
            smooth=0.0,
            eps=1e-8,
            ignore_index=None,
        )
        self.o["global_history"] = defaultdict(list)
        self.o["global_step"] = 0


class TrainCmd(ICommand):
    def __init__(self, o):
        self.o = o

    def execute(self):
        self.o["model"].to(self.o["device"])
        for epoch in range(self.o["epochs"]):
            for image, mask in self.o["dataloader.train"]:
                image = image.to(self.o["device"])
                mask = mask.to(self.o["device"])
                pred = self.o["model"](image)
                loss = self.o["loss_fn"](pred, mask.long())
                self.o[
                    "optimizer"
                ].zero_grad()  # clear gradients for this training step
                loss.backward()  # backpropagation, compute gradients
                self.o["optimizer"].step()  # apply gradients
                loss_value = loss.cpu().detach().numpy()
                self.o["global_history"]["loss"].append(loss_value)

                if self.o["global_step"] % 10 == 0:
                    print(
                        f"Epoch: {epoch}, "
                        f"global_step: {self.o['global_step']:06d}, "
                        f"train_loss: {loss_value:.6f}"
                    )

                if self.o["global_step"] % 100 == 0:
                    img = image[0, ...].permute(1, 2, 0).cpu().detach().numpy()
                    mask_true = mask[0, ...].cpu().detach().numpy()
                    mask_pred = pred[0, ...].softmax(0)
                    mask_pred = mask_pred.permute(1, 2, 0).cpu().detach().numpy()
                    # mask_pred = pred[0, ...].argmax(0).unsqueeze(0).unsqueeze(0)

                    # image = image[0, ...].permute(1, 2, 0).cpu().detach().numpy()
                    # mask = mask[0, ...].permute(1, 2, 0).cpu().detach().numpy()

                    fig, ax = plt.subplots(2, 8, figsize=(28, 6))
                    for i_col in range(7):
                        hax = ax[0, i_col].imshow(img[..., i_col])
                        divider = make_axes_locatable(ax[0, i_col])
                        cax = divider.append_axes("right", size="5%", pad=0.05)
                        fig.colorbar(hax, cax=cax, orientation="vertical")
                        hax = ax[1, i_col].imshow(
                            mask_pred[..., i_col + 1], vmin=0.9, vmax=1.0
                        )
                        divider = make_axes_locatable(ax[1, i_col])
                        cax = divider.append_axes("right", size="5%", pad=0.05)
                        fig.colorbar(hax, cax=cax, orientation="vertical")
                    ax[0, 7].imshow(img[..., 7])
                    divider = make_axes_locatable(ax[0, 7])
                    cax = divider.append_axes("right", size="5%", pad=0.05)
                    fig.colorbar(hax, cax=cax, orientation="vertical")
                    ax[1, 7].imshow(mask_true, vmin=0.9, vmax=1.0)
                    divider = make_axes_locatable(ax[1, 7])
                    cax = divider.append_axes("right", size="5%", pad=0.05)
                    fig.colorbar(hax, cax=cax, orientation="vertical")
                    plt.show()

                self.o["global_step"] += 1

            path_model_save = self.o["path.models"] / "model_full.pt"
            path_state_save = self.o["path.models"] / "model_state.pth"
            torch.save(self.o["model"], str(path_model_save))
            torch.save(self.o["model"].state_dict(), str(path_state_save))


if __name__ == "__main__":
    obj = UObject()
    InitTrainableObjectCmd(obj).execute()
    TrainCmd(obj).execute()
