# %%
import shutil
from pathlib import Path

import click


def unpack_archive(fn_in: str, path_out: str):
    """_summary_

    :param fn_in: _description_
    :param path_out: _description_
    :raises FileExistsError: _description_
    """
    fn_in = Path(fn_in)
    path_out = Path(path_out)

    if not fn_in.exists():
        raise FileExistsError()

    if not path_out.exists():
        path_out.mkdir(parents=True, exist_ok=True)

    shutil.unpack_archive(fn_in, path_out)


@click.command()
@click.argument("fn_in", type=click.Path())
@click.argument("path_out", type=click.Path())
def run_unpuck_archive(fn_in: click.Path, path_out: click.Path):
    unpack_archive(fn_in, path_out)


if __name__ == "__main__":
    # unpack_archive()
    import sys

    sys.argv = ["", "--fn_in", "mnist.zip", "--fn_out", "mnist-ubyte/"]
    path = Path("/home/rinkorn/space/prog/python/free/project-talantix-demo/")
    fn_in = path / "data/raw/mixed_train_to_the_coordinates_dataset.zip"
    fn_out = path / "data/processed/"
    unpack_archive(str(fn_in), str(fn_out))
